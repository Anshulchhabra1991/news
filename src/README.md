﻿PRODUCT API : 


End Points :

Route 1 :  /newsdesc/get
Type : post
Request : {
          	"country":"us",
          	"category":"business",
          	"keyword":"colleges"
          }
          optional-keyword
Response: [
              {
                  "Country": "us",
                  "Category": "business",
                  "Description": "Colleges and universities are continuing to re-evaluate their ties to John Schnatter, the founder of",
                  "Filter keyword": "business",
                  "News Title": "Colleges Cut Ties to Papa John's Founder After Slur",
                  "Source News URL": "https://www.insidehighered.com/quicktakes/2018/07/16/colleges-cut-ties-papa-johns-founder-after-slur"
              }
          ]
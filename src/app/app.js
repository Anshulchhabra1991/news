var express = require('express');
var bodyParser = require('body-parser');
var utility = require('./utility.js');
var constants = require('./constants.js');
var errorMessage = constants.errorMessage;
var redis = require('redis');
var client = redis.createClient();

client.on('connect', function() {
    console.log('Redis client connected');
    constants.redis_client = client;
});

var app = express();
app.use(bodyParser.json());

app.post('/newsdesc/get', function(req, res) {
    // var request_id = new ObjectID().toString();
    console.log("Endpoint : newsdesc/get    Request:",JSON.stringify(req.body));
    var body = req.body;
    if (!body) {
        errorMessage.result = "body not defined";
        return res.json(errorMessage);
    }
    if (!body.country) {
        errorMessage.result = "Country not defined";
        return res.json(errorMessage);
    }
    constants.country_name = body.country;
    if (!body.category) {
        errorMessage.result = "Category not defined";
        return res.json(errorMessage);
    }
    constants.category_name = body.category;
    if (body.keyword) {
        constants.news_api_keyword_name = body.keyword;
    }else{
        constants.news_api_keyword_name = "";
    }
    utility.validateConditionsAndGetData(body, function (err, response) {
        if(err){
            res.json(err);
        }else{
            res.json(response);
        }
    });
});

module.exports = app;
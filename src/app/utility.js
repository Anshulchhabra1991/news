var Constants = require('./constants.js');
var request = require('request');


var temp = module.exports = {

    validateConditionsAndGetData: function (body, callback) {
        var data = Constants.news_api_country_name+Constants.country_name+"&"+Constants.news_api_category_name+Constants.category_name+"&"+Constants.news_api_api_key;
        var finalUrl  = Constants.news_api_end_point+data;
        temp.getFromRedis(Constants.country_name,Constants.category_name,Constants.news_api_keyword_name,function (err,redisResponse) {
            if(err){
                return callback(err);
            }
            if(redisResponse.status){
                return callback("",redisResponse.result);
            }else{
                temp.getCall(finalUrl,{}, {},function (err, newsData) {
                    if (err) {
                        temp.setInRedis(Constants.country_name,Constants.category_name,Constants.news_api_keyword_name,err);
                        return callback(err)
                    }
                    if(newsData.totalResults > 0){
                        temp.filterResponse(Constants.country_name,Constants.category_name,newsData,0,[],function (error,finalResponse) {
                            if (finalResponse.length > 0) {
                                temp.setInRedis(Constants.country_name,Constants.category_name,Constants.news_api_keyword_name,finalResponse);
                                console.log("finalResponse is :")
                                console.log(finalResponse);
                                return callback("", finalResponse);
                            }
                            var errorMessage = Constants.successMessage;
                            errorMessage.result = "No Result found!!";
                            temp.setInRedis(Constants.country_name,Constants.category_name,Constants.news_api_keyword_name,errorMessage);
                            return callback(errorMessage,null);
                        });
                    }else{
                        var errorMessage = Constants.successMessage;
                        errorMessage.result = "No Result found!!";
                        temp.setInRedis(Constants.country_name,Constants.category_name,Constants.news_api_keyword_name,errorMessage);
                        return callback(errorMessage,null);
                    }
                });
            }
        });
    },

    getFromRedis: function(country,category,keyword,callback){
        var key  = country+"||"+category+"||"+keyword;
        Constants.redis_client.get(key,function (err,data) {
            if(err){
                console.log("getFromRedis() key:" +key+" data:"+data+" get response :error-"+err);
                return callback(err);
            }else if(data == null) {
                console.log("getFromRedis() key:" +key+" data:"+data);
                return callback("", {"status": false, "result": ""});
            }else{
                console.log("getFromRedis() key:" +key+" data:"+data);
                return callback("", {"status": true, "result": JSON.parse(data) });
            }
        })
    },

    setInRedis: function(country,category,keyword,response){
        var key  = country+"||"+category+"||"+keyword;
        Constants.redis_client.set(key,JSON.stringify(response),'EX',600,function(err,data){
            if(err){
                console.log("setInRedis() key:" +key+" data:"+data+" set response :error-"+err);
            }else{
                console.log("setInRedis() key:" +key+" data:"+data+" set response :success-"+data);
            }
        })
    },


    filterResponse: function(country,category,data,index,toReturnObj,callback) {
        var filterKeyword = Constants.news_api_keyword_name;
        if (index == undefined) {
            index = 0;
        }
        if (index < data.totalResults) {
            var obj = data['articles'][index] ;
            var keywordInDesc = 0;
            var keywordInTitle = 0;

            if(obj.description){
                keywordInDesc = obj.description.indexOf(filterKeyword) == -1?0:1;
            }
            if(obj.title){
                keywordInTitle = obj.title.indexOf(filterKeyword) == -1?0:1;
            }

            console.log("keywordInDesc");
            console.log(keywordInDesc);
            console.log("keywordInTitle");
            console.log(keywordInTitle);


            if (filterKeyword === "" || keywordInDesc || keywordInTitle ) {
                var tempObj = {};
                tempObj["Country"] = country;
                tempObj["Category"] = category;
                tempObj["Description"] = obj.description;
                if (obj.description != null) {
                    tempObj["Description"] = obj.description.substring(0, 100);
                }
                tempObj["Filter keyword"] = category;
                tempObj["News Title"] = obj.title;
                tempObj["Source News URL"] = obj.url;
                toReturnObj.push(tempObj);
                temp.filterResponse(country, category, data, index+1, toReturnObj, function (error,toReturnObj) {
                    return callback("",toReturnObj);
                });
            }else{
                temp.filterResponse(country, category, data, index+1, toReturnObj, function (error,toReturnObj) {
                    return callback("",toReturnObj);
                });
            }
        } else {
            return callback("", toReturnObj);
        }
    },

    getCall: function (url, data, headers, callback) {
        console.log("getCall() url: ", url, " data: ", JSON.stringify(data), " headers: ", JSON.stringify(headers));
        request({'headers': headers, 'url': url, 'qs': data, 'method': 'GET'}, function (err, res, body) {
            console.log("getCall() error: ", err, "  body: ", JSON.stringify(body));
            try{
                body = JSON.parse(body);
                if(body.status == "ok"){
                    return callback(null,body);
                }else{
                    return callback(body,null);
                }
            }catch (err) {
                callback({'status':false,'message':err.toString()}, null);
            }
        });
    }
}
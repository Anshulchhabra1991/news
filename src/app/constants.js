module.exports = Object.assign({

    errorMessage: {"status": "false", "result": "Please use correct API"},
    successMessage: {"status": "true", "result": []},
    news_api_end_point:"https://newsapi.org/v2/top-headlines?",
    news_api_country_name:"country=",
    country_name:"",
    news_api_category_name:"category=",
    category_name:"",
    news_api_api_key:"apiKey=eadb6da4bb5847a8b5f5b8a633e53ab9",
    news_api_keyword_name:"",
    news_api_total_error:"",

    redis_host:"127.0.0.1",
    redis_port:6379,
    redis_client:"",

});



'use strict';

var server = require('./app');

var port = process.env.APP_PORT || 4280;

server.listen(port, function() {
  console.log('Server running on port: %d', port);
});
